﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.DirectX;
using Microsoft.DirectX.AudioVideoPlayback;

namespace MAGplayer
{
    public partial class Form1 : Form
    {
        public Video globalVAr;
        public Form1()
        {

            InitializeComponent();
            button1.Text = "Open";
            button2.Text = "Play";
            label1.Text = "00:00:00";
            label2.Text = "00:00:00";
            button3.Text = "Forward";
            button4.Text = "Back";

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void _OpenFileDialog(object sender, EventArgs e)
        {
            string _filePath = "";
            OpenFileDialog o_f_d = new OpenFileDialog();
            o_f_d.Filter = "Файлы avi|*.avi";
            Video video;
            if (o_f_d.ShowDialog() == DialogResult.OK)
            {
                _filePath = o_f_d.FileName;
                openPlayVideo(_filePath);
               
            }
        }

        public void openPlayVideo(string _filepath)
        {
            var video = new Microsoft.DirectX.AudioVideoPlayback.Video(_filepath);
            globalVAr = video;
            video.Open(_filepath);
            video.Owner = panel1;
            panel1.Width = 720;
            panel1.Height = 384;
            trackBar1.Minimum = Convert.ToInt32(video.CurrentPosition);
            trackBar1.Maximum = Convert.ToInt32(video.Duration);
            int s = (int)video.Duration;
            int h = s / 3600;
            int m = (s - (h * 3600)) / 60;
            s = s - (h * 3600 + m * 60);

            label1.Text = String.Format("{0:D2}:{1:D2}:{2:D2}", h, m, s);
            video.Stop();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (globalVAr != null)
                if (globalVAr.Playing)
                {
                    globalVAr.Pause();
                    button2.Text = "Play";
                }
                else
                {
                    globalVAr.Play();
                    button2.Text = "Pause";
                }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            TimeVideo(trackBar1.Value);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if ((int)globalVAr.CurrentPosition + 70 < globalVAr.Duration)
                TimeVideo((int)globalVAr.CurrentPosition + 70);
            else
                TimeVideo((int)globalVAr.Duration);
        }

        private void TimeVideo(int var)
        {
            if (globalVAr != null)
            {
                globalVAr.CurrentPosition = var;
                trackBar1.Value=(int) globalVAr.CurrentPosition;
                int s = (int)globalVAr.CurrentPosition;
                int h = s / 3600;
                int m = (s - (h * 3600)) / 60;
                s = s - (h * 3600 + m * 60);
                label2.Text = String.Format("{0:D2}:{1:D2}:{2:D2}", h, m, s);
            }
            else
            {
                trackBar1.Value = 0;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if((int)globalVAr.CurrentPosition - 70 > 0)
                TimeVideo((int)globalVAr.CurrentPosition - 70);
            else
                TimeVideo(0);
        }
    }
}
